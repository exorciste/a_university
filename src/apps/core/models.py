from django.db import models
from django.utils.translation import ugettext_lazy as _


class Student(models.Model):
    first_name = models.CharField(_('Имя'), max_length=40)
    last_name = models.CharField(_('Фамилия'), max_length=40)
    age = models.PositiveIntegerField(_('Возраст'), null=True, blank=True)
    group = models.PositiveIntegerField(_('Группа'), null=True, blank=True)

    class Meta:
        verbose_name = _('Студент')
        verbose_name_plural = _('Студенты')

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)


class Subject(models.Model):
    student = models.ForeignKey(Student, verbose_name=_('Студент'), on_delete=models.SET_NULL, related_name='students',
                                null=True)
    name = models.CharField(_('Название предмета'), max_length=50)
    count_hour = models.PositiveIntegerField(_('Кол-во часов'), null=True, blank=True)
    teacher = models.CharField(_('Преподаватель'), max_length=50, blank=True)

    class Meta:
        verbose_name = _('Предмет')
        verbose_name_plural = _('Предметы')

    def __str__(self):
        return '{} : {}'.format(self.name, self.teacher)
