from django.contrib import admin
from django.contrib.auth.models import User, Group

from .models import Student, Subject

admin.site.unregister(User)
admin.site.unregister(Group)

admin.site.site_header = "University"
admin.site.site_title = "University"
admin.site.index_title = ""


class SubjectInlineAdmin(admin.TabularInline):
    model = Subject


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'age', 'group')
    inlines = [
        SubjectInlineAdmin
    ]


@admin.register(Subject)
class SubjectAdminX(admin.ModelAdmin):
    list_display = ('student', 'student_age', 'name', 'count_hour', 'teacher')

    def student_age(self, obj):
        return obj.student.age

    student_age.short_description = 'Возраст'
